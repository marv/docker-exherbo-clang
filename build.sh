#!/usr/bin/env bash
# vim: set sw=4 sts=4 ts=4 et tw=80 :

if [[ ${#} -lt 2 ]]; then
    echo "USAGE: ${0} <host> <slot>"
    exit 1
fi

HOST=${1}
SLOT=${2}
NEXT_SLOT=$(expr ${SLOT} + 1)

echo "Building LLVM version: ${SLOT} (masking >=${NEXT_SLOT})"

set -e

docker build \
    --no-cache \
    --rm \
    --pull \
    --build-arg HOST=${HOST} \
    --build-arg SLOT=${SLOT} \
    --build-arg NEXT_SLOT=${NEXT_SLOT} \
    -t marvins/exherbo-${HOST}-gcc-with-clang:${SLOT} \
    .

docker push marvins/exherbo-${HOST}-gcc-with-clang:${SLOT}
