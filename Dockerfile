ARG HOST

FROM exherbo/exherbo-${HOST}-base:latest
MAINTAINER Marvin Schmidt <marv@exherbo.org>

ARG HOST
ARG SLOT
ARG NEXT_SLOT

# Build generic binaries
COPY ./config/bashrc.${HOST} /etc/paludis/bashrc

# Unmasked development versions
COPY ./config/package_unmask.conf /etc/paludis
COPY ./config/package_mask.conf /etc/paludis
RUN sed -e "s/@SLOT@/${SLOT}/g" \
        -e "s/@NEXT_SLOT@/${NEXT_SLOT}/g" \
        -i /etc/paludis/package_{un,}mask.conf \
    && cat /etc/paludis/package_{un,}mask.conf

# Disable tests
# llvm:{11,12,13} on x86_64-pc-linux-musl:
#   - fails 7 CodeGen/Hexagon tests
# clang:12 on x86_64-pc-linux-musl:
#   Failed Tests (2):
#     Clang :: Driver/arm-float-abi-lto.c
#     Clang :: Driver/cuda-version-check.cu
# libc++ on x86_64-pc-linux-musl:
#   - fails a lot of tests
#   - testsuite checks for locale availability by trying `setlocale` with it, on
#     musl this always succeeds causing the testsuite to run all tests that
#     require a certain locale...
RUN mkdir -pv /etc/paludis/options.conf.d \
    && echo "dev-lang/llvm:11 build_options: -recommended_tests" >> /etc/paludis/options.conf.d/tests.conf \
    && echo "dev-lang/llvm:12 build_options: -recommended_tests" >> /etc/paludis/options.conf.d/tests.conf \
    && echo "dev-lang/llvm:13 build_options: -recommended_tests" >> /etc/paludis/options.conf.d/tests.conf \
    && echo "dev-lang/clang:12 build_options: -recommended_tests" >> /etc/paludis/options.conf.d/tests.conf \
    && echo "sys-libs/libc++ build_options: -recommended_tests" >> /etc/paludis/options.conf.d/tests.conf

RUN chgrp tty /dev/tty                               \
    && eclectic env update                           \
    && source /etc/profile                           \
    && cave sync                                     \
    && cave resolve -1z repository/{marv,python} -x  \
    && cave resolve -1z -Ks -ks -x \
        clang \
        compiler-rt \
        libc++ \
        libc++abi \
        lld \
        PyYAML \
    && rm /etc/paludis/package_{un,}mask.conf        \
    && rm -fr /etc/paludis/options.conf.d            \
    && rm -f /var/log/paludis/*                      \
    && rm -fr /var/cache/paludis/distfiles/*
